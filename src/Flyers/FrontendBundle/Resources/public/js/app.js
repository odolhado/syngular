'use strict';

// Declare app level module which depends on filters, and services
angular.module('myApp', ['myApp.filters', 'myApp.services', 'myApp.directives', 'myApp.controllers', 'ui.bootstrap']).
    config(['$routeProvider', function ($routeProvider) {
        $routeProvider
            .when('/login', {templateUrl: '/bundles/frontend/partials/login.html', controller: 'Login'})
            .when('/view1', {templateUrl: '/bundles/frontend/partials/partial1.html', controller: 'MyCtrl1'})
            .when('/view2', {templateUrl: '/bundles/frontend/partials/partial2.html', controller: 'MyCtrl2'})
            .when('/tables', {templateUrl: '/bundles/frontend/partials/tables.html', controller: ''})
            .when('/buttons', {templateUrl: '/bundles/frontend/partials/buttons.html', controller: ''})
            .when('/images', {templateUrl: '/bundles/frontend/partials/images.html', controller: ''})
            .when('/icons', {templateUrl: '/bundles/frontend/partials/icons.html', controller: ''})
            .when('/forms', {templateUrl: '/bundles/frontend/partials/forms.html', controller: ''})

            .when('/lesson2', {templateUrl: '/bundles/frontend/partials/bootstrap/lesson2.html', controller: ''})
            .when('/lesson3', {templateUrl: '/bundles/frontend/partials/bootstrap/lesson3.html', controller: ''})
            .when('/lesson4', {templateUrl: '/bundles/frontend/partials/bootstrap/lesson4.html', controller: ''})
            .when('/lesson5', {templateUrl: '/bundles/frontend/partials/bootstrap/lesson5.html', controller: ''})
            .when('/lesson6', {templateUrl: '/bundles/frontend/partials/bootstrap/lesson6.html', controller: ''})
            .when('/lesson7', {templateUrl: '/bundles/frontend/partials/bootstrap/lesson7.html', controller: ''})
            .when('/lesson8', {templateUrl: '/bundles/frontend/partials/bootstrap/lesson8.html', controller: ''})
            .when('/lesson9', {templateUrl: '/bundles/frontend/partials/bootstrap/lesson9.html', controller: ''})
            .when('/lesson10', {templateUrl: '/bundles/frontend/partials/bootstrap/lesson10.html', controller: ''})
            .when('/lesson11', {templateUrl: '/bundles/frontend/partials/bootstrap/lesson11.html', controller: ''})
            .when('/lesson12', {templateUrl: '/bundles/frontend/partials/bootstrap/lesson12.html', controller: ''})
            .when('/lesson13', {templateUrl: '/bundles/frontend/partials/bootstrap/lesson13.html', controller: ''})
            .when('/lesson14', {templateUrl: '/bundles/frontend/partials/bootstrap/lesson14.html', controller: ''})
            .when('/lesson15', {templateUrl: '/bundles/frontend/partials/bootstrap/lesson15.html', controller: ''})
            .when('/lesson16', {templateUrl: '/bundles/frontend/partials/bootstrap/lesson16.html', controller: ''})
            .when('/lesson17', {templateUrl: '/bundles/frontend/partials/bootstrap/lesson17.html', controller: ''})
            .when('/lesson18', {templateUrl: '/bundles/frontend/partials/bootstrap/lesson18.html', controller: ''})
            .when('/lesson19', {templateUrl: '/bundles/frontend/partials/bootstrap/lesson19.html', controller: ''})
            .when('/lesson20', {templateUrl: '/bundles/frontend/partials/bootstrap/lesson20.html', controller: ''})

            //$routeProvider.when('/form', {templateUrl: '/bundles/frontend/partials/Form.html', controller: 'Form'});
            .otherwise({redirectTo: '/login'});
        //$routeProvider.otherwise({redirectTo: '/blabla'});
    }]);
